(function(app) {

  app.AppComponent =
    ng.core.Component({
      selector: 'my-app',
      templateUrl: 'templates/app.component.html'
    })

    .Class({
      constructor: function() {}
    });

  var AppComponent = app.AppComponent;

  console.log("ng:");
  console.log(ng);
  app.RouterComponent =
    ng.router.RouteConfig([
      { path: '/', component: AppComponent }
    ])();
})(window.app || (window.app = {}));
